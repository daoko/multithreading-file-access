/**
 * The Reader thread reads the int from the database regardless of how many other threads are reading it
 */

import java.util.Random;


public class Reader implements Runnable {

	Random generator = new Random();
	char ID;
	Database db;
	
	public Reader(char ID, Database data){
		this.ID = ID;
		db = data;
		System.out.println("Created Reader with ID " + ID);
	}
	
	public void run() {
		System.out.println("Thread " + ID + " starting.");
		while(true){
			//This thread has entered it's critical section
			db.incReaders();
			
			//Sleep a little bit just to take up time
			try {
				Thread.sleep(generator.nextInt(300));
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			//Print out how many 
			System.out.println("Current Readers: " + db.getReaders() + " :: Reader " + ID + " read " + db.getData());
			//Exit the critical section
			db.decReaders();
			
			//Sleep a little more until the next  access attempt
			try {
				Thread.sleep(generator.nextInt(700));
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}