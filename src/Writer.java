/**
 * The Writer thread writes to the database
 * Unlike readers, only one writer can access the database at a time
 */
import java.util.Random;


public class Writer implements Runnable {

	Random generator = new Random();
	char ID;
	Database db;
	
	public Writer(char ID, Database data){
		this.ID = ID;
		db = data;
		System.out.println("Created Writer with ID " + ID);
	}
	

	public void run() {
		System.out.println("Thread " + ID + " starting.");
		while(true){
			//Check that the database is not locked
			if(db.getWriters() == 0){
				//Enter CS and lock database
				db.incWriters();
				//Generate new data
				int data = generator.nextInt(11);
				//Set the new data
				db.setData(data);
				
				System.out.println("Writer "+ ID + " Generated " + data);
				//Leave critical section
				db.decWriters();
				
				//Sleep until the next write
				try {
					Thread.sleep(generator.nextInt(1000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	


}
