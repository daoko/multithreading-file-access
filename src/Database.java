
/**
 * This is a simple class for keeping track of an integer of data, 
 * along with the number of readers and writers attempting to access it
 *
 */

public class Database {
	
	int writers=0, readers=0, data=0;
	
	public void setData(int i){
		data=i;
	}
	public int getData(){
		return data;
	}
	
	
	public int getReaders(){
		return readers;
	}	
	public void incReaders(){
		readers++;
	}
	public void decReaders(){
		readers--;
	}
	
	
	public int getWriters(){
		return writers;
	}
	public void incWriters(){
		writers++;
	}
	public void decWriters(){
		writers--;
	}

}
