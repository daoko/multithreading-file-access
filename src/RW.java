/**
 * Alex Case
 * Project 3: R/W Sync
 * 
 * This is the main class of the program. Creates the database and starts the threads
 */

import java.util.*;

public class RW {
	
	public static void main(String[] args) throws InterruptedException{

		Database db = new Database();
		
		int readers = Integer.parseInt(args[0]);
		int writers = Integer.parseInt(args[1]);
		int procs = readers + writers;
		System.out.println("Readers: " + readers + "\nWriters: " + writers + "\nTotal threads: " + procs);
		
		if((readers <0 || readers > 5) || (writers <0 || writers >5)){
			System.out.println("Maximum of 5 readers and 5 writers");
			System.exit(-1);
		}
		
		ArrayList<Thread> threads = new ArrayList<Thread>();
		char readerStart = 'A';
		char writerStart = 'F';
		
		for(int i=0; i<readers; i++){
			threads.add(new Thread(new Reader(readerStart, db)));
			readerStart++;
		}
		
		for(int i=0; i<writers; i++){
			threads.add(new Thread(new Writer(writerStart, db)));
			writerStart++;
		}

		for(Thread  t : threads){
			t.start();
		}
		
		
	}
}
